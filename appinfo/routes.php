<?php
/**
 * Create your routes in here. The name is the lowercase name of the controller
 * without the controller part, the stuff after the hash is the method.
 * e.g. page#index -> OCA\WorldPostaQuotaApi\Controller\PageController->index()
 *
 * The controller class has to be registered in the application.php file since
 * it's instantiated in there
 */
return [
    'routes' => [
        [
            'name' => 'quota_api#preflighted_cors',
            'url' => '/quota-api',
            'verb' => 'OPTIONS'
        ],
        ['name' => 'quota_api#index', 'url' => '/quota-api', 'verb' => 'PUT'],
        ['name' => 'quota_api#index', 'url' => '/quota-api', 'verb' => 'PATCH'],
    ]
];
