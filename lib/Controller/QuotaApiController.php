<?php

namespace OCA\WorldPostaQuotaApi\Controller;

use OC\Accounts\AccountManager;
use OC\HintException;
use OC\Settings\Mailer\NewUserMailHelper;
use OCA\Provisioning_API\FederatedFileSharingFactory;
use OCP\App\IAppManager;
use OCP\AppFramework\ApiController;
use OCP\AppFramework\Http\DataResponse;
use OCP\AppFramework\Http\JSONResponse;
use OCP\AppFramework\OCS\OCSException;
use OCP\AppFramework\OCS\OCSForbiddenException;
use OCP\IConfig;
use OCP\IGroup;
use OCP\IGroupManager;
use OCP\ILogger;
use OCP\IRequest;
use OCP\IUser;
use OCP\IUserManager;
use OCP\IUserSession;
use OCP\L10N\IFactory;
use OCP\Security\ISecureRandom;

class QuotaApiController extends ApiController
{

    private $userSession, $userManager, $groupManager;

    public function __construct(string $appName,
                                IRequest $request,
                                IUserManager $userManager,
                                IGroupManager $groupManager,
                                IUserSession $userSession)
    {
        parent::__construct($appName,
            $request);
        $this->userManager = $userManager;
        $this->userSession = $userSession;
        $this->groupManager = $groupManager;
    }


    /**
     *
     * @NoCSRFRequired
     * edit users
     *
     * @param string $userId
     * @param string $key
     * @param string $value
     *
     * @throws OCSException
     */
    public function index($data)
    {


        $currentLoggedInUser = $this->userSession->getUser();

        if (!$this->groupManager->isAdmin($currentLoggedInUser->getUID())) {
            throw new OCSException('', \OCP\API::RESPOND_UNAUTHORISED);
        }

        $dataCopy = [];
        try {
            foreach ($data as $email => $newQuota) {

                $targetUser = $this->userManager->getByEmail($email);
                if (count($targetUser) > 0 && $targetUser[0] !== null) {
                    $targetUser = $targetUser[0];
                    $dataCopy[$email] = $targetUser->getQuota();
                    $this->updateUser($targetUser, $newQuota);
                }

            }
        } catch (\Exception $e) {
            foreach ($dataCopy as $email => $newQuota) {
                $targetUser = $this->userManager->getByEmail($email);
                $this->updateUser($targetUser, $newQuota);
            }
            return new JSONResponse(['status' => 'failed'], 500);
        }


        return new JSONResponse(['status' => 'success'], 200);
    }

    private function updateUser($targetUser, $newQuota)
    {
        $quota = $newQuota;
        if ($quota !== 'none' && $quota !== 'default') {
            if (is_numeric($quota)) {
                $quota = (float)$quota;
            } else {
                $quota = \OCP\Util::computerFileSize($quota);
            }
            if ($quota === false) {
                throw new OCSException('Invalid quota value ' . $newQuota, 103);
            }
            if ($quota === -1) {
                $quota = 'none';
            } else {
                $quota = \OCP\Util::humanFileSize($quota);
            }
        }
        $targetUser->setQuota($quota);

    }

}