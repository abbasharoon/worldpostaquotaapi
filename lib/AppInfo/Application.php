<?php

namespace OCA\WorldPostaQuotaApi\AppInfo;

use OCA\WorldPostaQuotaApi\Controller\QuotaApiController;
use OCP\AppFramework\App;


class Application extends App
{

    public function __construct(array $urlParams = array())
    {
        parent::__construct('worldpostaquotaapi', $urlParams);

        $container = $this->getContainer();

        /**
         * Controllers
         */
        $container->registerService('QuotaApiController', function ($c) {
            return new QuotaApiController(
                $c->query('AppName'),
                $c->query('Request'),
                $c->query('UserManager'),
                $c->query('GroupManager'),
                $c->query('UserSession')
            );
        });

        $container->registerService('GroupManager', function ($c) {
            return $c->query('ServerContainer')->getGroupManager();
        });
        $container->registerService('UserManager', function ($c) {
            return $c->query('ServerContainer')->getUserManager();
        });
        $container->registerService('UserSession', function ($c) {
            return $c->query('ServerContainer')->getUserSession();
        });

        // currently logged in user, userId can be gotten by calling the
        // getUID() method on it
        $container->registerService('User', function ($c) {
            return $c->query('UserSession')->getUser();
        });


    }
}